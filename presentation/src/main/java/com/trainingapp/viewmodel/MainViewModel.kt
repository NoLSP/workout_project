package com.trainingapp.viewmodel

import androidx.lifecycle.ViewModel
import com.moduleTrainingApp.data.repositories.UserRepository

class MainViewModel(private val userRepository: UserRepository) : ViewModel()
{
    fun userIsEmpty(): Boolean =  userRepository.profileIsEmpty()
}