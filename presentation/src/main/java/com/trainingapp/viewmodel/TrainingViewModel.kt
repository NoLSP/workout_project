package com.trainingapp.viewmodel

import android.app.Application
import android.os.CountDownTimer
import androidx.lifecycle.*
import com.moduleTrainingApp.data.AppDatabase
import com.moduleTrainingApp.data.entities.Step
import com.moduleTrainingApp.data.entities.Training
import com.moduleTrainingApp.data.impl.TrainingRepository
import com.moduleTrainingApp.data.repositories.TrainingRepInterface
import com.moduleTrainingApp.data.repositories.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.trainingapp.R
import java.text.SimpleDateFormat
import java.util.*

class TrainingViewModel(application: Application,
                        private val userRepository: UserRepository) : ViewModel()
{
    private val trainingRepository: TrainingRepInterface

    private lateinit var training: Training
    private var currentTimer: CountDownTimer? = null
    private var stepCounter: Int = 0

    private val finishMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val currentStepMutableLiveData: MutableLiveData<Step> = MutableLiveData()
    private val navActionMutableLiveData: MutableLiveData<Int> = MutableLiveData()
    private val timerMutableLiveData: MutableLiveData<String> = MutableLiveData()

    var currentStep: LiveData<Step> = currentStepMutableLiveData
    var trainingIsFinish: LiveData<Boolean> = finishMutableLiveData
    var navAction: LiveData<Int> = navActionMutableLiveData
    val timer: LiveData<String> = timerMutableLiveData

    init
    {
        val chinupsDao = AppDatabase.getDatabase(application).chinupsDao()
        val pushupsDao = AppDatabase.getDatabase(application).pushupsDao()
        val situpsDao = AppDatabase.getDatabase(application).situpsDao()
        val legupsDao = AppDatabase.getDatabase(application).legupsDao()
        val dumbbellsDao = AppDatabase.getDatabase(application).dumbbellsDao()
        trainingRepository =
                TrainingRepository(chinupsDao, pushupsDao, situpsDao, legupsDao, dumbbellsDao)

        val currentUserWorkload = userRepository.workload

        viewModelScope.launch {
            withContext(Dispatchers.IO)
            {
                training = trainingRepository.getTraining(currentUserWorkload!!)
                currentStepMutableLiveData.postValue(training.StepList[stepCounter])
            }
        }
    }

    fun rest()
    {
        navActionMutableLiveData.value = R.id.action_setsFragment_to_restFragment
        currentTimer = createTimer().run { start() }
    }

    fun next()
    {
        currentTimer?.cancel()

        stepCounter++

        if (stepCounter < training.StepList.size)
            currentStepMutableLiveData.value = training.StepList[stepCounter]
        else
        {
            finish()
            finishMutableLiveData.value = true
        }

        navActionMutableLiveData.value = R.id.action_restFragment_to_setsFragment
    }

    fun getCurrentNumOfStep(): Int
    {
        return stepCounter + 1
    }

    private fun finish()
    {
        userRepository.workload?.let {
            it.Abs++
            it.Chest++
            it.Legs++
            it.Triceps++
        }
    }

    private fun createTimer(): CountDownTimer
    {
        return object : CountDownTimer((training.RestTime * 60000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long)
            {
                timerMutableLiveData.value = SimpleDateFormat("mm:ss").format(Date(millisUntilFinished))
            }

            override fun onFinish()
            {
                next()
            }

        }
    }
}