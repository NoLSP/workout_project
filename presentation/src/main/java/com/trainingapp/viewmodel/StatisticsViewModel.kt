package com.trainingapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.moduleTrainingApp.data.AppDatabase
import com.moduleTrainingApp.data.entities.StatisticsEntity
import com.moduleTrainingApp.data.impl.StatisticsRepository
import com.moduleTrainingApp.data.repositories.StatisticsRepInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StatisticsViewModel(application: Application) : AndroidViewModel(application)
{
    private val statisticsRepository : StatisticsRepInterface

    init
    {
        val statisticsDao = AppDatabase.getDatabase(application).statisticsDao()
        statisticsRepository = StatisticsRepository(statisticsDao)
    }

    fun addStatistic(date : Long, chinupsSum : Int, pushupsSum : Int, situpsSum : Int, legupsSum : Int, dumbbellsSum : Int)
    {
        GlobalScope.launch (Dispatchers.IO) {
            statisticsRepository.addStatistic(date, chinupsSum, pushupsSum, situpsSum, legupsSum, dumbbellsSum)
        }
    }

    fun getStatistics(): LiveData<List<StatisticsEntity>> = statisticsRepository.statistics
}