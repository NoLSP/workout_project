package com.trainingapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.moduleTrainingApp.data.repositories.UserRepository
import com.moduleTrainingApp.data.entities.User

class EditProfileViewModel(application: Application,
                           private val userRepository: UserRepository) : AndroidViewModel(application)
{
    fun setUserProfile(user: User)
    {
        userRepository.apply {
            name = user.name
            height = user.height
            weight = user.weight
            workload = user.workload
        }
    }
}