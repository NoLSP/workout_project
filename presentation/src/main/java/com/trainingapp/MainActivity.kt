package com.trainingapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.moduleTrainingApp.data.AppDatabase
import com.moduleTrainingApp.data.impl.UserRepositiryImpl
import com.trainingapp.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : FragmentActivity()
{
    lateinit var navController: NavController
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainViewModel(UserRepositiryImpl(this@MainActivity)) as T
            }
        }).get(MainViewModel::class.java)

        navController = findNavController(R.id.fragment)
    }

    override fun onStart()
    {
        super.onStart()

        if (viewModel.userIsEmpty()) {
            val db = AppDatabase.buildDatabase(this.applicationContext)
            navController.navigate(R.id.editProfileFragment)
        }
        else
            navController.navigate(R.id.startFragment)
    }
}