package com.trainingapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.moduleTrainingApp.data.impl.UserRepositiryImpl
import com.trainingapp.R
import com.trainingapp.viewmodel.TrainingViewModel
import kotlinx.android.synthetic.main.fragment_rest.*
import kotlinx.android.synthetic.main.fragment_rest.view.*

class RestFragment : Fragment()
{
    private lateinit var sharedViewModel: TrainingViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        val view = inflater.inflate(R.layout.fragment_rest, container, false)

        val parentFragment = requireParentFragment().requireParentFragment()
        sharedViewModel = ViewModelProvider(parentFragment, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return TrainingViewModel(requireActivity().application, UserRepositiryImpl(requireActivity())) as T
            }
        }).get(TrainingViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.button_rest.setOnClickListener {
            sharedViewModel.next()
        }

        sharedViewModel.timer.observe(viewLifecycleOwner)
        {
            text_restTimer.text = it
        }
    }
}