package com.trainingapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.moduleTrainingApp.data.impl.UserRepositiryImpl
import com.trainingapp.R
import com.trainingapp.viewmodel.TrainingViewModel
import kotlinx.android.synthetic.main.fragment_sets.view.*

class SetsFragment : Fragment()
{
    private lateinit var sharedViewModel: TrainingViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        val view = inflater.inflate(R.layout.fragment_sets, container, false)

        val parentFragment = requireParentFragment().requireParentFragment()
        sharedViewModel = ViewModelProvider(parentFragment, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return TrainingViewModel(requireActivity().application, UserRepositiryImpl(requireActivity())) as T
            }
        }).get(TrainingViewModel::class.java)

        view.tv_step_num.text = sharedViewModel.getCurrentNumOfStep().toString()

        sharedViewModel.currentStep.observe(viewLifecycleOwner) { step ->
            if (step.Pushups != 0)
                view.apply {
                    pushups_layout.visibility = View.VISIBLE
                    pushups_count.text = step.Pushups.toString()
                }

            if (step.Situps != 0)
                view.apply {
                    situps_layout.visibility = View.VISIBLE
                    situps_count.text = step.Situps.toString()
                }

            if (step.Chinups != 0)
                view.apply {
                    chinups_layout.visibility = View.VISIBLE
                    chinups_count.text = step.Chinups.toString()
                }

            if (step.Dumbbells != 0)
                view.apply {
                    dumbbells_layout.visibility = View.VISIBLE
                    dumbbells_count.text = step.Dumbbells.toString()
                }

            if (step.Legups != 0)
                view.apply {
                    legups_layout.visibility = View.VISIBLE
                    legups_count.text = step.Legups.toString()
                }
        }

        //push up - Ожтимания
        //sit up - press
        //chin ups - подтягивания
        //leg ups - подтягивание ног
        //dumlss гантели

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        view.button_next.setOnClickListener {
            sharedViewModel.rest()
        }
    }
}