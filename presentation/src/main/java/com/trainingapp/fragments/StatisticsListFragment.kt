package com.trainingapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.trainingapp.R
import com.trainingapp.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_statistics_list.*

class StatisticsListFragment : Fragment()
{
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_statistics_list, container, false)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        //пока чисто для теста
        recyclerView.adapter = RecyclerAdapter(listOf("21 Декабря 10:00", "22 Декабря 11:35", "23 Декабря 14:00"))

        return view
    }
}