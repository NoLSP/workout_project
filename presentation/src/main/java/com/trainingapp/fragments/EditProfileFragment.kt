package com.trainingapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.moduleTrainingApp.data.impl.UserRepositiryImpl
import com.moduleTrainingApp.data.entities.User
import com.moduleTrainingApp.data.entities.Workload
import com.trainingapp.MainActivity
import com.trainingapp.R
import com.trainingapp.viewmodel.EditProfileViewModel
import kotlinx.android.synthetic.main.fragment_edit_profile.view.*

class EditProfileFragment : Fragment()
{
    private lateinit var viewModel: EditProfileViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_edit_profile, container, false)

        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return EditProfileViewModel(requireActivity().application,
                        UserRepositiryImpl(requireActivity())) as T
            }
        }).get(EditProfileViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        view.button_set.setOnClickListener {
            val name = view.et_name.text.toString()
            val height = view.et_height.text.toString()
            val weight = view.et_weight.text.toString()

            if (name.isEmpty() || height.isEmpty() || weight.isEmpty())
            {
                Snackbar.make(view, "Введите все данные", Snackbar.LENGTH_LONG).show()
            }
            else
            {
                val wlChest = view.sb_wl_chest.progress
                val wlAbs = view.sb_wl_abs.progress
                val wlLegs = view.sb_wl_legs.progress
                val wlTriceps = view.sb_wl_triceps.progress
                viewModel.setUserProfile(User(
                        name,
                        height.toInt(),
                        weight.toInt(),
                        Workload(
                                wlChest,
                                wlAbs,
                                wlTriceps,
                                wlLegs
                        )
                ))

                (requireActivity() as MainActivity).navController
                        .navigate(R.id.action_ePFragment_to_startFragment)
            }
        }
    }
}