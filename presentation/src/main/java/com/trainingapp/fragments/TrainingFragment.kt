package com.trainingapp.fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.moduleTrainingApp.data.impl.UserRepositiryImpl
import com.trainingapp.MainActivity
import com.trainingapp.R
import com.trainingapp.viewmodel.TrainingViewModel
import kotlinx.android.synthetic.main.dialog_finish.*

class TrainingFragment : Fragment()
{
    private lateinit var trainingViewModel: TrainingViewModel

    lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_training, container, false)

        trainingViewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return TrainingViewModel(requireActivity().application, UserRepositiryImpl(requireActivity())) as T
            }
        }).get(TrainingViewModel::class.java)

        val navHostFragment = childFragmentManager.findFragmentById(R.id.fragment2) as NavHostFragment
        navController = navHostFragment.navController

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        trainingViewModel.trainingIsFinish.observe(viewLifecycleOwner)
        { _ ->
            showFinishDialog()
            (requireActivity() as MainActivity).navController
                    .navigate(R.id.action_trainingFragment_to_startFragment)
        }

        trainingViewModel.navAction.observe(viewLifecycleOwner)
        { action ->
            Log.i("ACTION", action.toString())
            navController.navigate(action)
        }
    }

    private fun showFinishDialog()
    {
        Dialog(requireContext()).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.dialog_finish)
            button_finish.setOnClickListener {
                dismiss()
            }
        }.show()
    }

}