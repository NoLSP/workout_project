package com.trainingapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trainingapp.MainActivity
import com.trainingapp.R
import kotlinx.android.synthetic.main.fragment_start.view.*

class StartFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        val view = inflater.inflate(R.layout.fragment_start, container, false)

        view.button_start.setOnClickListener {
            (requireActivity() as MainActivity).navController
                .navigate(R.id.action_startFragment_to_trainingFragment)
        }

        return view
    }
}