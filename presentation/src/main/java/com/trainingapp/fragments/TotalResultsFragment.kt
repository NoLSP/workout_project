package com.trainingapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.trainingapp.R
import kotlinx.android.synthetic.main.fragment_total_results.view.*

class TotalResultsFragment : Fragment()
{
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_total_results, container, false)
        //todo прописать логику получения данных
        view.finish_button.setOnClickListener {
            //todo
        }

        return view
    }
}