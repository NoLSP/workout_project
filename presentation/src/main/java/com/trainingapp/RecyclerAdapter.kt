package com.trainingapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(private val values : List<String>) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>()
{
    override fun getItemCount() = values.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.recycler_item, parent,false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.dateTimeTextView?.text = values[position]
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)
    {
        var dateTimeTextView : TextView? = null
        var descriptionTextView : TextView? = null
        var moreInfoButton : Button? = null

        init {
            dateTimeTextView = itemView.findViewById(R.id.tv_datetime)
            descriptionTextView = itemView.findViewById(R.id.tv_description)
            moreInfoButton = itemView.findViewById(R.id.more_info_button)
        }
    }
}