package com.moduleTrainingApp.data.repositories

import com.moduleTrainingApp.data.entities.Workload

interface UserRepository
{
    var name: String?
    var height: Int?
    var weight: Int?
    var workload: Workload?

    fun profileIsEmpty(): Boolean
}