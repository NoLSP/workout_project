package com.moduleTrainingApp.data.repositories

import com.moduleTrainingApp.data.entities.Training
import com.moduleTrainingApp.data.entities.Workload

interface TrainingRepInterface
{
    suspend fun getTraining(workload : Workload) : Training
}