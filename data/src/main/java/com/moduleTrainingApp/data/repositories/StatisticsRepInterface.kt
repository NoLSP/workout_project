package com.moduleTrainingApp.data.repositories

import androidx.lifecycle.LiveData
import com.moduleTrainingApp.data.entities.StatisticsEntity

interface StatisticsRepInterface {
    val statistics : LiveData<List<StatisticsEntity>>
    fun addStatistic(date : Long, chinupsSum : Int, pushupsSum : Int, situpsSum : Int, legupsSum : Int, dumbbellsSum : Int)
}