package com.moduleTrainingApp.data.impl

import com.moduleTrainingApp.data.dao.*
import com.moduleTrainingApp.data.entities.Step
import com.moduleTrainingApp.data.entities.Training
import com.moduleTrainingApp.data.entities.Workload
import com.moduleTrainingApp.data.repositories.TrainingRepInterface

class TrainingRepository(private val chinupsDao: ChinupsDao,
                         private val pushupsDao: PushupsDao,
                         private val situpsDao: SitupsDao,
                         private val legupsDao: LegupsDao,
                         private val dumbbellsDao: DumbbellsDao) : TrainingRepInterface
{

    override suspend fun getTraining(workload: Workload): Training
    {
            //Отдых пока задается костыльно
            val restTime = 2

            val chinups = chinupsDao.getChinupEntity(workload.Triceps)
            val pushups = pushupsDao.getPushupsEntity(workload.Chest)
            val situps = situpsDao.getSitupsEntity(workload.Abs)
            val legups = legupsDao.getLegupsEntity(workload.Legs)
            val dumbbels = dumbbellsDao.getDumbbellsEntity(workload.Chest)

            //Это конечно страшно
            val step1 = Step(chinups.step1, dumbbels.step1, legups.step1, pushups.step1, situps.step1)
            val step2 = Step(chinups.step2, dumbbels.step2, legups.step2, pushups.step2, situps.step2)
            val step3 = Step(chinups.step3, dumbbels.step3, legups.step3, pushups.step3, situps.step3)
            val step4 = Step(chinups.step4, dumbbels.step4, legups.step4, pushups.step4, situps.step4)
            val step5 = Step(chinups.step5, dumbbels.step5, legups.step5, pushups.step5, situps.step5)
            val step6 = Step(chinups.step6, dumbbels.step6, legups.step6, pushups.step6, situps.step6)
            val step7 = Step(chinups.step7, dumbbels.step7, legups.step7, pushups.step7, situps.step7)
            val step8 = Step(chinups.step8, dumbbels.step8, legups.step8, pushups.step8, situps.step8)
            val step9 = Step(chinups.step9, dumbbels.step9, legups.step9, pushups.step9, situps.step9)
            val step10 = Step(chinups.step10, dumbbels.step10, legups.step10, pushups.step10, situps.step10)
            val step11 = Step(chinups.step11, dumbbels.step11, legups.step11, pushups.step11, situps.step11)
            val step12 = Step(chinups.step12, dumbbels.step12, legups.step12, pushups.step12, situps.step12)
            val step13 = Step(chinups.step13, dumbbels.step13, legups.step13, pushups.step13, situps.step13)
            val step14 = Step(chinups.step14, dumbbels.step14, legups.step14, pushups.step14, situps.step14)
            val step15 = Step(chinups.step15, dumbbels.step15, legups.step15, pushups.step15, situps.step15)

            val stepList: MutableList<Step> = mutableListOf<Step>(step1, step2, step3, step4, step5,
                    step6, step7, step8, step9, step10)
            if (step11.Situps != 0) {
                stepList.addAll(mutableListOf(step11, step12))
            }
            if (step13.Situps != 0) {
                stepList.addAll(mutableListOf(step13, step14, step15))
            }

            return Training(stepList, restTime)
    }
}