package com.moduleTrainingApp.data.impl

import androidx.lifecycle.LiveData
import com.moduleTrainingApp.data.dao.StatisticsDao
import com.moduleTrainingApp.data.entities.StatisticsEntity
import com.moduleTrainingApp.data.repositories.StatisticsRepInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StatisticsRepository(private val statisticsDao: StatisticsDao) : StatisticsRepInterface
{
    override val statistics : LiveData<List<StatisticsEntity>> = statisticsDao.getAll()

    override fun addStatistic(date : Long, chinupsSum : Int, pushupsSum : Int, situpsSum : Int, legupsSum : Int, dumbbellsSum : Int)
    {
        GlobalScope.launch(Dispatchers.IO) {
        statisticsDao.addStatistic(StatisticsEntity(1,date, chinupsSum, pushupsSum, situpsSum, legupsSum, dumbbellsSum))
        }
    }
}