package com.moduleTrainingApp.data.impl

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.moduleTrainingApp.data.entities.Workload
import com.moduleTrainingApp.data.repositories.UserRepository

class UserRepositiryImpl(activity: Activity) : UserRepository
{
    private val USER_INFO_PREF_KEY = "USER_INFO_PREF"
    private val userInfoPreferences: SharedPreferences

    private val USER_NAME_KEY = "USER_NAME"
    private val USER_HEIGHT_KEY = "USER_HEIGHT"
    private val USER_WEIGHT_KEY = "USER_WEIGHT"
    private val USER_CHEST_KEY = "USER_CHEST"
    private val USER_ABS_KEY = "USER_ABS"
    private val USER_TRICEPS_KEY = "USER_TRICEPS"
    private val USER_LEGS_KEY = "USER_LEGS"

    init
    {
        userInfoPreferences = activity.getSharedPreferences(USER_INFO_PREF_KEY, Context.MODE_PRIVATE)
    }

    override var name: String?
        get() = userInfoPreferences.getString(USER_NAME_KEY, null)
        set(value)
        {
            userInfoPreferences.edit()
                    .putString(USER_NAME_KEY, value)
                    .apply()
        }

    override var height: Int?
        get() =
            if (userInfoPreferences.contains(USER_HEIGHT_KEY))
                userInfoPreferences.getInt(USER_HEIGHT_KEY, 0)
            else
                null
        set(value)
        {
            value?.let {
                userInfoPreferences.edit()
                        .putInt(USER_HEIGHT_KEY, it)
                        .apply()
            }
        }

    override var weight: Int?
        get() =
            if (userInfoPreferences.contains(USER_WEIGHT_KEY))
                userInfoPreferences.getInt(USER_WEIGHT_KEY, 0)
            else
                null
        set(value)
        {
            value?.let {
                userInfoPreferences.edit()
                        .putInt(USER_WEIGHT_KEY, it)
                        .apply()
            }
        }

    override var workload: Workload?
        get() =
            if (userInfoPreferences.contains(USER_CHEST_KEY)
                    && userInfoPreferences.contains(USER_ABS_KEY)
                    && userInfoPreferences.contains(USER_LEGS_KEY)
                    && userInfoPreferences.contains(USER_TRICEPS_KEY))
            {
                Workload(
                        userInfoPreferences.getInt(USER_CHEST_KEY, 0),
                        userInfoPreferences.getInt(USER_ABS_KEY, 0),
                        userInfoPreferences.getInt(USER_TRICEPS_KEY, 0),
                        userInfoPreferences.getInt(USER_LEGS_KEY, 0)
                )
            }
            else
                null
        set(value)
        {
            value?.let {
                userInfoPreferences.edit()
                        .putInt(USER_CHEST_KEY, it.Chest)
                        .putInt(USER_ABS_KEY, it.Abs)
                        .putInt(USER_TRICEPS_KEY, it.Triceps)
                        .putInt(USER_LEGS_KEY, it.Legs)
                        .apply()
            }
        }

    override fun profileIsEmpty(): Boolean
    {
        Log.i(this.toString(), "$name $height $weight $workload")

        return name == null ||
                height == null ||
                weight == null ||
                workload == null
    }
}