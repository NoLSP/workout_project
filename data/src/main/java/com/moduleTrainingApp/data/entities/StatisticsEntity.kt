package com.moduleTrainingApp.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Statistics")
class StatisticsEntity(@PrimaryKey(autoGenerate = true) var id: Int, var date : Long, var chinupsSum : Int,
                       var pushupsSum : Int, var situpsSum : Int, var legupsSum : Int, var dumbbellsSum : Int)
{
}