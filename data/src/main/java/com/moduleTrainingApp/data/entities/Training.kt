package com.moduleTrainingApp.data.entities

data class Training(val StepList : List<Step>, val RestTime : Int )