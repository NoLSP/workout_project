package com.moduleTrainingApp.data.entities

import androidx.room.Entity

@Entity(tableName = "Situps", primaryKeys = ["day"])
data class SitupsEntity(val day : Int, val step1 : Int, val step2 : Int,
                         val step3 : Int, val step4 : Int, val step5 : Int, val step6 : Int, val step7 : Int,
                         val step8 : Int, val step9 : Int, val step10 : Int, val step11 : Int, val step12 : Int,
                         val step13 : Int, val step14 : Int, val step15 : Int)