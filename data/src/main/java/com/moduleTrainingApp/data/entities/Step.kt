package com.moduleTrainingApp.data.entities

data class Step(val Chinups : Int, val Dumbbells : Int, val Legups : Int, val Pushups : Int, val Situps : Int)