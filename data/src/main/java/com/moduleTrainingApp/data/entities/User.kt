package com.moduleTrainingApp.data.entities

data class User(
        val name: String,
        val height: Int,
        val weight: Int,
        val workload: Workload
)