package com.moduleTrainingApp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.moduleTrainingApp.data.dao.*
import com.moduleTrainingApp.data.entities.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

@Database(entities = [ChinupsEntity::class, PushupsEntity::class,
                     LegupsEntity::class, SitupsEntity::class, DumbbellsEntity::class, StatisticsEntity::class],
            version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun statisticsDao(): StatisticsDao

    abstract fun chinupsDao(): ChinupsDao
    abstract fun pushupsDao(): PushupsDao
    abstract fun dumbbellsDao(): DumbbellsDao
    abstract fun situpsDao(): SitupsDao
    abstract fun legupsDao(): LegupsDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{

            if (INSTANCE == null){
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "Database").build()
                }
            }

            return INSTANCE!!
        }

         fun buildDatabase(context: Context)
         {
             GlobalScope.launch(Dispatchers.IO) {
                 getDatabase(context).chinupsDao().insertAll(CHINUPS_DATA)
                 getDatabase(context).pushupsDao().insertAll(PUSHUPS_DATA)
                 getDatabase(context).situpsDao().insertAll(SITUPS_DATA)
                 getDatabase(context).legupsDao().insertAll(LEGUPS_DATA)
                 getDatabase(context).dumbbellsDao().insertAll(DUMBBELLS_DATA)
             }
         }

        val CHINUPS_DATA = listOf(ChinupsEntity(1, 6, 0, 5, 0, 5, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(2, 6, 0, 5, 0, 5, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(3, 6, 0, 5, 0, 5, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(5, 6, 0, 5, 0, 5, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(6, 6, 0, 5, 0, 5, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(8, 7, 0, 6, 0, 5, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(9, 7, 0, 6, 0, 5, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(10, 7, 0, 6, 0, 5, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(12, 7, 0, 6, 0, 5, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(13, 7, 0, 6, 0, 5, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(15, 8, 0, 6, 0, 5, 0, 5, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(16, 8, 0, 6, 0, 5, 0, 5, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(17, 8, 0, 6, 0, 5, 0, 5, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(19, 8, 0, 6, 0, 5, 0, 5, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(20, 8, 0, 6, 0, 5, 0, 5, 0, 4, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(22, 8, 0, 7, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(23, 8, 0, 7, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(24, 8, 0, 7, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(26, 8, 0, 7, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(27, 8, 0, 7, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0),
                ChinupsEntity(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

        val PUSHUPS_DATA = listOf(PushupsEntity(1,10,12,8,8,12,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(2,10,12,8,8,12,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(3,10,12,8,8,12,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(5,11,15,9,9,13,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(6,11,15,9,9,13,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(8,11,15,9,9,13,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(9,11,15,9,9,13,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(10,11,15,9,9,13,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(12,14,14,10,10,15,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(13,14,14,10,10,15,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(15,14,14,10,10,15,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(16,14,14,10,10,15,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(17,14,14,10,10,15,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(19,16,17,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(20,16,17,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(22,16,17,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(23,16,17,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(24,16,17,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(26,14,18,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(27,14,18,14,14,20,0,0,0,0,0,0,0,0,0,0),
                PushupsEntity(28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))


        val SITUPS_DATA = listOf(SitupsEntity(1,10,20,25,10,25,20,20,25,10,10,0,0,0,0,0),
                SitupsEntity(2,10,20,25,10,25,20,20,25,10,10,0,0,0,0,0),
                SitupsEntity(3,10,20,25,10,25,20,20,25,10,10,0,0,0,0,0),
                SitupsEntity(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(5,10,30,25,30,15,25,20,20,25,10,0,0,0,0,0),
                SitupsEntity(6,10,30,25,30,15,25,20,20,25,10,0,0,0,0,0),
                SitupsEntity(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(8,10,30,25,30,15,25,20,20,25,10,0,0,0,0,0),
                SitupsEntity(9,10,30,25,30,15,25,20,20,25,10,0,0,0,0,0),
                SitupsEntity(10,10,30,25,30,15,25,20,20,25,10,0,0,0,0,0),
                SitupsEntity(11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(12,15,25,35,30,10,20,15,20,10,20,0,0,0,0,0),
                SitupsEntity(13,15,25,35,30,10,20,15,20,10,20,0,0,0,0,0),
                SitupsEntity(14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(15,15,25,35,30,10,20,15,20,10,20,0,0,0,0,0),
                SitupsEntity(16,15,25,35,30,10,20,15,20,10,20,0,0,0,0,0),
                SitupsEntity(17,15,25,35,30,10,20,15,20,10,20,0,0,0,0,0),
                SitupsEntity(18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(19,20,10,30,25,20,10,30,30,15,10,0,0,0,0,0),
                SitupsEntity(20,20,10,30,25,20,10,30,30,15,10,0,0,0,0,0),
                SitupsEntity(21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(22,20,10,30,25,20,10,30,30,15,10,0,0,0,0,0),
                SitupsEntity(23,0,10,30,25,20,10,30,30,15,10,0,0,0,0,0),
                SitupsEntity(24,20,10,30,25,20,10,30,30,15,10,0,0,0,0,0),
                SitupsEntity(25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                SitupsEntity(26,30,25,10,20,15,35,20,25,10,10,0,0,0,0,0),
                SitupsEntity(27,30,25,10,20,15,35,20,25,10,10,0,0,0,0,0),
                SitupsEntity(28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))

        val DUMBBELLS_DATA = listOf(DumbbellsEntity(1,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(2,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(3,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(5,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(6,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(8,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(9,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(10,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(12,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(13,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(15,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(16,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(17,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(19,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(20,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                DumbbellsEntity(21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(22,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                DumbbellsEntity(23,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                DumbbellsEntity(24,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                DumbbellsEntity(25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                DumbbellsEntity(26,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                DumbbellsEntity(27,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                DumbbellsEntity(28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))

        val LEGUPS_DATA = listOf(LegupsEntity(1,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(2,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(3,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(5,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(6,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(8,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(9,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(10,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(12,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(13,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(15,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(16,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(17,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(19,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(20,0,12,0,10,0,10,0,8,0,12,0,0,0,0,0),
                LegupsEntity(21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(22,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                LegupsEntity(23,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                LegupsEntity(24,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                LegupsEntity(25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
                LegupsEntity(26,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                LegupsEntity(27,0,14,0,12,0,10,0,10,0,14,0,0,0,0,0),
                LegupsEntity(28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    }
}