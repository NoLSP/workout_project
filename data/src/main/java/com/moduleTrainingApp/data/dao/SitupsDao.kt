package com.moduleTrainingApp.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.SitupsEntity

@Dao
interface SitupsDao
{
    @Query("SELECT * FROM Situps WHERE day = :day")
    suspend fun getSitupsEntity(day : Int) : SitupsEntity

    @Query("SELECT * FROM Situps")
    suspend fun getAll() : List<SitupsEntity>

    @Insert
    fun insertAll(data : List<SitupsEntity>)
}