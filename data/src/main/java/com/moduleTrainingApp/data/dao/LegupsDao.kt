package com.moduleTrainingApp.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.LegupsEntity

@Dao
interface LegupsDao
{
    @Query("SELECT * FROM Legups WHERE day = :day")
    suspend fun getLegupsEntity(day : Int) : LegupsEntity

    @Query("SELECT * FROM Legups")
    suspend fun getAll() : List<LegupsEntity>

    @Insert
    fun insertAll(data : List<LegupsEntity>)
}