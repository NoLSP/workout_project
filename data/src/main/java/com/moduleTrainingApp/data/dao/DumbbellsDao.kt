package com.moduleTrainingApp.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.DumbbellsEntity

@Dao
interface DumbbellsDao
{
    @Query("SELECT * FROM Dumbbells WHERE day = :day")
    suspend fun getDumbbellsEntity(day : Int) : DumbbellsEntity

    @Query("SELECT * FROM Dumbbells")
    suspend fun getAll() : List<DumbbellsEntity>

    @Insert
    fun insertAll(data : List<DumbbellsEntity>)
}