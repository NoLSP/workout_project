package com.moduleTrainingApp.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.PushupsEntity

@Dao
interface PushupsDao
{
    @Query("SELECT * FROM Pushups WHERE day = :day")
    suspend fun getPushupsEntity(day : Int) : PushupsEntity

    @Query("SELECT * FROM Pushups")
    suspend fun getAll() : List<PushupsEntity>

    @Insert
    fun insertAll(data : List<PushupsEntity>)
}