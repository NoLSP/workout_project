package com.moduleTrainingApp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.StatisticsEntity

@Dao
interface StatisticsDao
{
    @Query("SELECT * FROM Statistics")
    fun getAll() : LiveData<List<StatisticsEntity>>

    @Insert
    suspend fun addStatistic(statics : StatisticsEntity)
}