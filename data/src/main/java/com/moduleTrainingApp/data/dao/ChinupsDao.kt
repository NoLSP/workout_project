package com.moduleTrainingApp.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.moduleTrainingApp.data.entities.ChinupsEntity

@Dao
interface ChinupsDao
{
    @Query("SELECT * FROM Chinups WHERE day = :day")
    suspend fun getChinupEntity(day : Int) : ChinupsEntity

    @Query("SELECT * FROM Chinups")
    suspend fun getAll() : List<ChinupsEntity>

    @Insert
    fun insertAll(data : List<ChinupsEntity>)
}